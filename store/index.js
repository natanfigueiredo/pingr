export const state = () => ({
    token: ''
})

export const mutations = {
    token(state, token) {
        state.token = token
    }
}

